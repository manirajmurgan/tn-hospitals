import { useEffect, useState } from "react";
import Select from "react-select";

import ReactGA from 'react-ga';

ReactGA.initialize('G-CMRHR7V3KQ');



const App = () => {
  const [districtList, setDistrictList] = useState([]);

  const [selectedDistrict, setSelectedDistrict] = useState({});

  const [hospitals, setHospitals] = useState([]);

  const [errMsg, setErrMsg] = useState(false);

  useEffect(() => {

    ReactGA.pageview(window.location.pathname + window.location.search);

    fetch(`https://tncovidbeds.tnega.org/api/district`)
      .then((res) => res.json())
      .then((districts) => {
        setDistrictList(districts && districts.result);
      })
      .catch((err) => {
        setErrMsg(true);
      });
  }, []);

  const handleDistrictChange = (selectedDistrict) => {
    setSelectedDistrict((prev) => {
      getHospitalData(selectedDistrict);
      return { ...prev, ...selectedDistrict };
    });
  };

  const getHospitalData = (district) => {
    const data = {
      Districts: [district._id],
      FacilityTypes: ["CHO", "CHC", "CCC"],
      IsGovernmentHospital: true,
      IsPrivateHospital: true,
      SortValue: "Availability",
      pageLimit: 100,
      pageNumber: 1,
      searchString: "",
      sortCondition: { Name: 1 },
    };

    fetch("https://tncovidbeds.tnega.org/api/hospitals", {
      method: "post",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((res) => res.json())
      .then((res) => setHospitals(res.result))
      .catch((err) => {
        setErrMsg(true);
      });
  };

  return (
    <>
      <header className="text-gray-600 body-font">
        <div className="container mx-auto justify-center flex flex-wrap p-5 flex-col md:flex-row items-center shadow-lg">
          <a className="flex order-first lg:order-none lg:w-1/5 title-font font-medium items-center text-gray-900 lg:items-center lg:justify-center md:mb-0">
            <span className="ml-3 text-xl">Tamil Nadu COVID Bed Details</span>
          </a>
        </div>
      </header>
      <p className="leading-relaxed p-4 text-center">
        Re-developed and Issued in public interest by{" "}
        <br />
        <a
          href="https://karurtech.com/"
          target="_blank"
          className="text-indigo-500"
        >
          {" "}
          Karur Tech Solutions (KTS){" "}
        </a>
        <img className="w-20 mx-auto" src="kts_logo.jpeg" alt="kts-logo"/>
      </p>
      <h2 className="font-bold text-md text-center mb-2">
        {" "}
        Our Special Thanks to,{" "}
      </h2>
      <div className="flex flex-wrap mx-4 text-center border-b-2 border-gray-200 mb-4">
        <div className="p-1 w-2/4">
          <img className="w-4/6 md:w-1/2 mx-auto" src="cm_tn.jpg" alt="cm_logo" />
          <a
            className="leading-relaxed text-md text-indigo-500"
            href="https://twitter.com/mkstalin"
            target="_blank"
          >
            Chief Minister of Tamilnadu
          </a>
        </div>
        <div className="p-1 w-1/4">
          <img className="w-40 mx-auto" src="elc_min.jpg" alt="minister_logo" />
          <a
            className="text-left leading-relaxed text-xs text-indigo-500"
            href="https://twitter.com/vsenthilbalaji"
            target="_blank"
          >
            Minister of Electricity, Tamilnadu.
          </a>
        </div>
        <div className="p-1 w-1/4">
          <div className="">
            <img className="w-40 mx-auto" src="health_min.jpg" alt="minister_logo" />
            <a
              className="leading-relaxed text-xs text-indigo-500"
              href="https://twitter.com/vsenthilbalaji"
              target="_blank"
            >
              Minister of Health, Tamilnadu.
            </a>
          </div>
        </div>
      </div>
      <p className="text-center text-md px-4">
        {" "}
        Please search for any district name below, you will get respective
        hospital data along with <span className="font-semibold"> Bed availaibilty, Contact details and Map location.{" "} </span>
      </p>
      <div className="container px-4 py-8">
        {districtList && districtList.length > 0 && (
          <Select
            onChange={handleDistrictChange}
            options={districtList}
            value={districtList.filter(
              (district) => district._id === selectedDistrict._id
            )}
            placeholder={<div>Select / Search for district here</div>}
            getOptionLabel={(option) => option.Name}
            getOptionValue={(option) => option._id}
          />
        )}
      </div>
      <div className="font bold leading-relaxed my-4 px-4 w-full text-center">
        {selectedDistrict && selectedDistrict.Name
          ? `${selectedDistrict?.Name} / ${selectedDistrict?.TamilName}`
          : null}
      </div>
      {errMsg ? (
        <div role="alert">
          <div className="bg-red-500 text-white font-bold rounded-t px-4 py-2">
            Sorry
          </div>
          <div className="border border-t-0 border-red-400 rounded-b bg-red-100 px-4 py-3 text-red-700">
            <p>Something not ideal might be happening.</p>
          </div>
        </div>
      ) : null}

      {hospitals &&
        hospitals.length > 0 &&
        hospitals.map((hospital, index) => {
          if (hospital.Name) {
            return (
              <div
                key={index}
                className="h-full border-2 border-gray-200 border-opacity-60 shadow-lg overflow-hidden cursor-pointer p-2 m-4"
              >
                <h1 className="title-font text-lg font-bold jl-card-title text-gray-900 mb-3">
                  {hospital.Name}
                </h1>
                <p className="mt-2 text-md text-gray-900">
                  {hospital?.AddressDetail?.Line1}
                  <br />
                  {hospital?.AddressDetail?.Line2}
                  <br />
                  {hospital?.AddressDetail?.Line3}
                </p>
                <table className="table-fixed border my-10 w-full">
                  <thead>
                    <tr>
                      <th className="w-1/3 ...">Bed Type</th>
                      <th className="w-1/3 ...">Vacant</th>
                      <th className="w-1/3 ...">Occupied</th>
                      <th className="w-1/3 ...">Total</th>
                    </tr>
                  </thead>
                  <tbody className="text-center py-4">
                    <tr>
                      <td>Normal</td>
                      <td> {hospital?.CovidBedDetails?.VaccantNonO2Beds} </td>
                      <td> {hospital?.CovidBedDetails?.OccupancyNonO2Beds} </td>
                      <td> {hospital?.CovidBedDetails?.AllotedNonO2Beds} </td>
                    </tr>
                    <tr>
                      <td>Oxygen</td>
                      <td> {hospital?.CovidBedDetails?.VaccantO2Beds} </td>
                      <td> {hospital?.CovidBedDetails?.OccupancyO2Beds} </td>
                      <td> {hospital?.CovidBedDetails?.AllotedO2Beds} </td>
                    </tr>
                    <tr>
                      <td>ICU</td>
                      <td> {hospital?.CovidBedDetails?.VaccantICUBeds} </td>
                      <td> {hospital?.CovidBedDetails?.OccupancyICUBeds} </td>
                      <td> {hospital?.CovidBedDetails?.AllotedICUBeds} </td>
                    </tr>
                    <br />
                    <tr className="mt-2">
                      <td colSpan={3}>
                        {" "}
                        Total Beds in Hospital <br /> (Vacant + Occupied +
                        additions(if any))
                      </td>
                      <td>{hospital?.CovidBedDetails?.TotalBedsInHospital} </td>
                    </tr>
                  </tbody>
                </table>
                <p className="mt-2 text-md text-gray-500 w-full text-center">
                  Last Updated on:{" "}
                  {new Date(
                    +`${hospital?.CovidBedDetails?.LastUpdatedTime}000`
                  ).toLocaleString("en-GB")}
                </p>
                <h1></h1>
                <table className="table-fixed border my-10 w-full">
                  <thead>
                    <tr>
                      <th className="w-1/3 ...">Contact Name</th>
                      <th className="w-1/3 ...">Contact Number</th>
                    </tr>
                  </thead>
                  <tbody className="text-center py-4">
                    <tr>
                      <td> {hospital?.ContactDetails[0]?.ContactName} </td>
                      <td> {hospital?.ContactDetails[0]?.ContactNumber} </td>
                    </tr>
                  </tbody>
                </table>
                <table className="table-fixed border my-10 w-full">
                  <tbody className="text-center py-4">
                    <tr className="p-4">
                      <td className="w-1/4">
                        {" "}
                        Click on the phone icon to dial.
                      </td>
                      <td>
                        <a
                          href={`tel:${hospital?.ContactDetails[0]?.ContactNumber}`}
                        >
                          <img
                            className="w-1/3 mx-auto"
                            src="smartphone.svg"
                            alt="phone-icon"
                          />
                        </a>
                      </td>
                      <td className="w-1/4">
                        {" "}
                        Click on the location icon to get the map location.
                      </td>
                      <td>
                        <a
                          href={`http://maps.google.com/?q=${hospital?.Latitude},${hospital?.Longitude}`}
                          target="_blank"
                        >
                          <img
                            className="w-1/3 mx-auto"
                            src="map.svg"
                            alt="map-icon"
                          />
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            );
          }
        })}
    </>
  );
};

export default App;
